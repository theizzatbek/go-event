package main

import "fmt"

func main() {

	type person struct {
		name string
		age  int
		pet  string
	}

	john := person{
		"John",
		40,
		"cat",
	}

	fmt.Println(john)
}
