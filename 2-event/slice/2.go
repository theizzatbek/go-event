package main

import "fmt"

func main() {

	var x []int

	fmt.Println(x == nil) // prints true
	
	x = append(x, 10)
	fmt.Println(x[0])

	x[0] = 5
	fmt.Println(x[0])
}
