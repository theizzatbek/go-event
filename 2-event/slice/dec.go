package main

import "fmt"

func dec() {
	// START OMIT
	var x1 []int 

	var x2 = []int{10, 20, 30} 

	var x3 = []int{1, 2: 20, 4: 40} 

	var x4 = make([]int, 5) 
	//END OMIT

	fmt.Println(x1, x2, x3, x4)
}


