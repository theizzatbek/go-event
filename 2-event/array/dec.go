package main

import "fmt"

func dec() {
	// START OMIT
	var x1 [3]int 

	var x2 = [3]int{10, 20, 30}

	var x3 = [5]int{1, 2: 20, 4: 40} 

	var x4 = [...]int{1, 2, 3} 
	//END OMIT

	fmt.Println(x1, x2, x3, x4)
}