package main

import "fmt"

func compare() {
	// START OMIT
	var x = [...]int{1, 2, 3}

	var y = [3]int{1, 2, 3}

	fmt.Println(x == y) // prints true
	//END OMIT

	fmt.Println(x, y)
}
